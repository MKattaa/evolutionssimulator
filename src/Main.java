import java.util.Random;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    // Das Programm starten
    public static void main(String[] args) {
        launch(args);
    }

    Field field = new Field();

    public boolean s = false;                  // s für starten

    public boolean details = false;

    // start = 0, wenn das Programm noch nicht gestartet ist
    public int start = 0;

    // Bild für das Essen
    Image foodImage = new Image(getClass().getResourceAsStream("/images/food.png"));

    // Wir haben dieses Objekt verwenden, damit in jedem Durchlauf alle Komponenten neu auf dem Bild gezeigt werden.
    // Jedes 0,1 Sec wird ein neuen Durchlauf gemacht.
    Timeline timeline = new Timeline();

    //Zählern
    public int oneSecCounter = 0;
    public int timeCounter = 0;
    public int helpCountforGeneration = 10;

    // Hilfsobjekt um Random-Positionen zu erstellen
    Random random = new Random();

    public int richtung;           // Richtung der Bewegung bestimmen (0 -> Rechts; 1 -> Links; 2 -> Oben; 3 -> Unten; 4 -> Keine Bewegung)

    // Die Position des ersten Essens feststellen
    public int xPos = random.nextInt(22);
    public int yPos = 5 + random.nextInt(17);
    Food firstFood = new Food(new Position(field.foodXPos[xPos],field.foodYPos[yPos]));

    //Ein Hilfsarray für die Postionen der ersten 3 Individuen
    final int[] defaultX = new int[4];
    final int[] defaultY = new int[4];

    //Der Inhalt der Benutzeroberfläche zeichnen
    private void drawShapes(GraphicsContext gc) {
        //Default Individuen bzw. Essen erstellen
        if (start == 0) {
            defaultX[2] = 40;
            defaultX[1] = 160;
            defaultX[0] = 180;

            defaultY[2] = 100;
            defaultY[1] = 150;
            defaultY[0] = 200;

            if(field.indis.size() < 3) {
                for (int i = 0; i < field.countOfIndividual; i++) {
                    field.indis.add(new Individual(field.defaultSpeed, 3, field.defaultSense, defaultX[i], defaultY[i]));
                }
            }

            field.foodListe[0] = firstFood;
            timeCounter = 0;
            timeline.play();
        }


        // Die Generation inkrementieren
        if (field.countOfIndividual/helpCountforGeneration >= 1) {
            field.generation++;
            helpCountforGeneration += 10;

            //Die Generation speichern
            field.writeInTheFile();
        }

        // Die Hintergrundfarbe ist schwarz
        gc.setFill(Color.BLACK);
        gc.fillRect(0, 0, 750, 500);

        // Der Rahmen zeichen
        gc.setFill(Color.color(0.2, 0.2, 0.2));
        for (int i = 6; i <= 482; i += 17) {
            for (int j = 6; j <= 482; j += 17) {
                gc.fillRect(i, j, 13, 13);
            }
        }

        // Subhintergrund mit schwarz färben
        gc.setFill(Color.BLACK);
        gc.fillRect(20, 20, 460, 460);

        // Der Titel des Projekts
        gc.setFill(Color.CYAN);
        gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gc.fillText("Evolution Simulator", 510, 35);

        // Laufzeit
        gc.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        gc.fillText("Laufzeit:              " + timeCounter, 550, 280);

        //Niveau Erklärung
        gc.setFill(new Color(0, 0, 1, 1));          //Am langsamsten
        gc.fillOval(510, 297, 20, 20);

        gc.setFill(new Color(0, 1, 1, 1));          //eher langsam
        gc.fillOval(560, 297, 20, 20);

        gc.setFill(new Color(0, 1, 0, 1));          //Normalstand
        gc.fillOval(610, 297, 20, 20);

        gc.setFill(new Color(1, 1, 0, 1));          //eher schnell
        gc.fillOval(660, 297, 20, 20);

        gc.setFill(new Color(1, 0, 0, 1));          //Am schnellsten
        gc.fillOval(710, 297, 20, 20);

        gc.setFill(Color.LIGHTGRAY);
        gc.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
        gc.fillText("Am                      Normal-                    Am", 510, 337);
        gc.fillText("langsamsten          stand         schnellsten", 510, 357);

        //Alles andere mit LIGHTGRAY färben
        gc.setFill(Color.LIGHTGRAY);

        //Informationen über das Projekt zeigen und die benötigte GUI-Komponenten entwurfen
        gc.setFont(Font.font("Arial", FontWeight.NORMAL, 18));

        gc.fillText("Generation", 578, 65);
        gc.fillRect(550, 75, 140, 30);
        gc.setFill(Color.BLACK);
        gc.fillRect(551, 76, 138, 28);
        gc.setFill(Color.LIGHTGRAY);
        gc.fillText(field.generation + "", 550 + (142 - new Text(field.generation + "").getLayoutBounds().getWidth()) / 2, 97);

        gc.fillText("Anzahl der Individuen", 532, 140);
        gc.fillRect(550, 150, 140, 30);
        gc.setFill(Color.BLACK);
        gc.fillRect(551, 151, 138, 28);
        gc.setFill(Color.LIGHTGRAY);
        gc.fillText(field.indis.size() + "", 550 + (142 - new Text(field.indis.size() + "").getLayoutBounds().getWidth()) / 2, 172);

        gc.fillText("Anzahl der Essen", 550, 215);
        gc.fillRect(550, 225, 140, 30);
        gc.setFill(Color.BLACK);
        gc.fillRect(551, 226, 138, 28);
        gc.setFill(Color.LIGHTGRAY);
        gc.fillText(field.countOfFood + "", 550 + (142 - new Text(field.countOfFood + "").getLayoutBounds().getWidth()) / 2, 247);

        gc.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        gc.fillText("Controls", 520, 385);

        gc.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        gc.fillText("Starten / Anhalten : Space", 520, 410);
        gc.fillText("Starten : S", 520, 435);
        gc.fillText("Reseten : R", 620, 435);
        gc.fillText("Individuum addieren : I", 520, 460);
        gc.fillText("Essen addieren : F", 520, 485);
        gc.fillText("Details der Individuen : D", 520, 510);

        if(details)
        {
            gc.fillText("Details", 20, 535);
            gc.setFill(Color.BLACK);
            gc.fillRect(21, 551, 980, 178);
            gc.setFill(Color.LIGHTGRAY);
            gc.fillRect(20, 550, 960, 180);
            gc.setFill(Color.BLACK);
            gc.fillRect(21, 551, 958, 178);
            gc.setFill(Color.LIGHTGRAY);
            int i = 1;
            for(Individual individual: field.indis)
            {
                if(i < 8)
                {
                    String detailOfIndi = "Individual " + i + " ;  Speed = " + individual.speed + " ; Energy supply = " + individual.energySupply + " ; Size = " + individual.size + " ; Sense = " + individual.sense ;
                    gc.fillText(detailOfIndi, 25, 545 + i * 25);
                    i++;
                }
            }

        }


        // Timer wird in jeder Sekunde um eins erhöht
        if (oneSecCounter == 9) {
            if (timeCounter != 1000) {
                timeCounter++;
                oneSecCounter = 0;
            }
        } else {
            oneSecCounter++;
        }

        //Die Individuen auf dem Bildschirm ausgeben
        for (Individual individual: field.indis){
            gc.setFill(individual.color);
            gc.fillOval(individual.getIndividualPosX(), individual.getIndividualPosY(), 20, 20);
        }

        //wenn s gedrückt ist, dann die Bewegung starten.
        if (s) {
            for (Individual individual : field.indis) {
                richtung = random.nextInt(4);               //Die Richtung in jedem Durchlauf bestimmen
                individual.move(richtung);
            }
        }

        //Überprüfen, ob einen Individuum gestorben ist
        for (Individual individual : field.indis) {
            if (individual.energie <= 0){
                field.removeIndividual(individual);
                field.countOfIndividual--;
            }
        }
        // Überprüfen, ob ein Individuum ein anderer essen kann und wenn ja, dann essen.
        for (Individual individual1: field.indis){
            for (Individual individual2: field.indis){
                if (individual1 != individual2){
                    if (between((float) individual2.getIndividualPosX(), individual1.individualPosX+individual1.sense, individual1.individualPosX-individual1.sense)&&between((float) individual2.getIndividualPosY(), individual1.individualPosY+individual1.sense, individual1.individualPosY-individual1.sense)) {

                        if (individual1.isEatable(individual2) && individual1.replicateCount >= 2) {
                            field.removeIndividual(individual2);
                            individual1.eat();
                        }
                    }
                }
            }
        }

        //Überprüfen, ob einen Individuum sich vermehren kann
        for (Individual individual : field.indis) {
            if (individual.canReplicate && individual.replicateCount < 2){
                field.indis.add(individual.replicate(richtung, 20, 20));
                field.countOfIndividual++;
            }
        }

        //Überprüfen, ob die Individuen etwas zum Essen gefunden haben
        for (Individual individual: field.indis) {
            for (int i = 0; i < field.countOfFood; i++) {

                if (between((float) field.foodListe[i].position.x, individual.individualPosX+individual.sense, individual.individualPosX-individual.sense)&&between((float) field.foodListe[i].position.y, individual.individualPosY+individual.sense, individual.individualPosY-individual.sense)){
                    field.gesammtAnzahlFoodEaten++;
                    individual.eat();
                    xPos = random.nextInt(22);
                    yPos = random.nextInt(22);
                    field.foodListe[i].position.x = field.foodXPos[xPos];
                    field.foodListe[i].position.y = field.foodYPos[yPos];
                }
            }
        }

        //Für die Sicherheit, dass das Essen nicht auf dem Individuum generiert wird.
        for (int i = 0; i < field.indis.size(); i++) {
            for (int j = 0; j<field.countOfFood; j++) {
                if (field.indis.get(i).getIndividualPosX() == field.foodListe[j].position.x && field.indis.get(i).getIndividualPosY() == field.foodListe[j].position.y) {
                    xPos = random.nextInt(22);
                    yPos = random.nextInt(22);
                    field.foodListe[j].position.x = field.foodXPos[xPos];
                    field.foodListe[j].position.y = field.foodYPos[yPos];
                }
            }
        }

        //Das Essen auf dem Bildschirm zeigen
        for (int i = 0; i<field.countOfFood; i++) {
            gc.drawImage(foodImage, field.foodListe[i].position.x, field.foodListe[i].position.y);
        }

        if(timeCounter >= 30)
        {
            field.generation += 1;
            timeCounter = 0;
            for (Individual individual : field.indis) {

                if(individual.eatCounter == 0)
                {
                    field.indis.remove(individual);
                }
                individual.eatCounter = 0;
                individual.replicateCount = 0;
                individual.energie = 100000;
            }
        }
    }
    
    @Override
    public void start(Stage stage) {

        //Grund-Eigenschaften der Benutzeroberfläche bestimmen
        Canvas canvas = new Canvas(1000, 750);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Pane root = new Pane();
        root.setStyle("-fx-background-color: black;");
        root.getChildren().add(canvas);
        Scene scene = new Scene(root);
        stage.setTitle("Evolutionssimulator");
        stage.setScene(scene);
        stage.show();

        // Jedes 0,1 Sekunde wird die Methode drawShapes durch das Attribut timeline aufgerufen
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(0.1), (ActionEvent event) -> {
            drawShapes(gc);
        }));

        //entsrecht einer Repeat-Methode
        timeline.setCycleCount(Timeline.INDEFINITE);

        // Die Methode play aufrufen
        timeline.play();

        // Die Ereignisse bestimmen
        scene.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent e) -> {

            if (null != e.getCode())
            {
                switch (e.getCode())
                {
                    case SPACE:
                        // Das Programm anhalten
                        if (timeline.getStatus() == Timeline.Status.RUNNING)
                        {
                            timeline.stop();
                            s = false;
                        }
                        // Das Programm wieder starten
                        else if (timeline.getStatus() != Timeline.Status.RUNNING)
                        {
                            timeline.play();
                            start++;
                            s = true;
                        }
                        break;

                    case R:             //Reset
                        timeline.play();
                        start = 0;
                        field.generation = 0;
                        field.gesammtAnzahlFoodEaten = 0;
                        field.countOfIndividual = 3;
                        s = false;
                        xPos = random.nextInt(22);
                        yPos = 5 + random.nextInt(17);
                        field.indis.clear();
                        break;

                    case I:             //Individuen addieren
                        field.neueIndividualleAddieren();
                        break;

                    case F:             //Essen addieren
                        field.newFood();
                        break;

                    case S:                //Starten
                        timeline.play();
                        start++;
                        s = true;
                        break;

                    case D:                //Details
                        if(details == false)
                        {
                            details = true;
                        }
                        else
                        {
                            details = false;
                        }
                        break;
                }
            }
        });
    }

    //Hilfsmethode "between" (Zahlenbereich)
    public boolean between(float i, float maxValueInclusive, float minValueInclusive) {
        if (i >= minValueInclusive && i <= maxValueInclusive)
            return true;
        else
            return false;
    }
}
