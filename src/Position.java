public class Position {
 
    // Standort auf der X bzw. Y Achse
    int x, y;
 
    // Position auf der X bzw. Y Achse erstellen
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
