import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class Field
{

    ArrayList<Individual> indis = new ArrayList<>();
    Food [] foodListe = new Food[30];                   // Nur bis 30 Food-Objekte können addiert werden

    public int generation = readfromTheFile();

    // Zählern
    public int countOfIndividual = 3;
    public int gesammtAnzahlFoodEaten = 0;
    public int countOfFood = 1;

    //Default Eigenschaften des Individuals
    public double defaultSpeed = 1.0;
    public double defaultSize = 10.0;
    public int defaultSense = 20;

    // Plätze für des Essen
    int[] foodXPos = {20, 40, 60, 80, 100, 120, 140, 160, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460};
    int[] foodYPos = {20, 40, 60, 80, 100, 120, 140, 160, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460};

    public Field()
    {

    }

    //Methoden für Individauum bzw. Essen addieren
    public void neueIndividualleAddieren() {

        if (countOfIndividual < 35) { // Man kann nur bis 35 Individual-Objekte einfügen
            Random random = new Random();
            int x = 20 + random.nextInt(400);
            int y = 20 + random.nextInt(400);

            indis.add(new Individual(defaultSpeed, defaultSize, defaultSense, x, y));
            countOfIndividual++;
        }
    }

    public void removeIndividual(Individual a)
    {
        indis.remove(a);
        countOfIndividual --;
    }

    public void newFood() {

        if (countOfFood < 30) {
            Random random = new Random();
            int xPos = random.nextInt(22);
            int yPos = random.nextInt(17);
            Food food = new Food(new Position(foodXPos[xPos], foodYPos[yPos]));
            foodListe[countOfFood] = food;
            countOfFood++;
        }
    }

    // Methoden, um die generation zurückzusetzen
    public void writeInTheFile() {
            try {
                FileOutputStream fos = new FileOutputStream("./generationlevel.txt");
                OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
                osw.write(generation + "");
                osw.flush();
                osw.close();
            } catch (IOException e) {
            }
    }

    public int readfromTheFile() {
        try {
            InputStreamReader isr = new InputStreamReader(new FileInputStream("./generationlevel.txt"), "UTF-8");
            BufferedReader br = new BufferedReader(isr);

            String str = "";
            int c;
            while ((c = br.read()) != -1) {
                if (Character.isDigit(c))
                    str += (char) c;
            }
            if (str.equals(""))
                str = "0";

            br.close();
            return Integer.parseInt(str);
        } catch (IOException e) {
        }
        return 0;
    }
}

