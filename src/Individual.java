import javafx.scene.paint.Color;
import java.util.Random;

public class Individual {

    //Eigenschaften des Individuums
    public double speed;
    public double energySupply;
    public double size;
    public int sense;
    public Color color;
    public int energie = 100000;

    public boolean hasEaten = false;
    public int eatCounter = 0;
    public boolean canReplicate = false;
    public int replicateCount = 0;

    //Standort des Individuums
    public int individualPosX;
    public int individualPosY;

    int richtung;

    public Individual(double speed, double size, int sense, int individualPosX,int individualPoY)
    {
        this.speed = speed;
        this.size = size;
        this.sense = sense;

        this.individualPosX = individualPosX;
        this.individualPosY = individualPoY;

        this.energySupply = ((speed * speed) * (size * size * size)) + sense;

        this.color = makeColor();
    }

    //Die get-Methoden
    public double getSize() {
        return size;
    }
    public int getIndividualPosX() {
        return individualPosX;
    }
    public int getIndividualPosY() {
        return individualPosY;
    }


    public void move(int richtung)
    {
        this.richtung = richtung;

        //Right
        if (richtung == 0)
        {
            individualPosX += (20 * speed);

            if (individualPosX > 460)
                individualPosX = 20;
        }
        else if (richtung == 1) {        //Left
            individualPosX -= (20 * speed);

            if (individualPosX < 20)
                individualPosX = 460;

        }
        else if (richtung == 2)        //Up
        {
            individualPosY-= (20 * speed);

            if (individualPosY  < 20)
                individualPosY = 460;

        }
        else if (richtung == 3)          //Down
        {
            individualPosY+= (20 * speed);

            if (individualPosY  > 460)
                individualPosY = 20;
        }

        if(eatCounter == 0)
        {
            energie -= energySupply;
        }
    }

    public void eat(){

        if (eatCounter == 0) {
            eatCounter++;
            size += 2;
            speed -= 0.1;
            hasEaten = true;
        } else if (eatCounter == 1) {
            eatCounter++;
            size++;
            if (speed >= 0.1) {
                speed -= 0.05;
            }
            canReplicate = true;
        } else {
            size++;
            eatCounter++;
            canReplicate = true;
            if (speed >= 0.1) {
                speed -= 0.05;
            }
        }
        this.energySupply = ((speed * speed) * (size * size * size)) + sense;

        this.color = makeColor();
    }

    public boolean isEatable(Individual otherIndividual){

        boolean isEatable = false;
        if(otherIndividual.getSize() <= size/2)
        {
            isEatable = true;
        }

        return isEatable;
    }

    public Individual replicate(int posVal, int maxPosX, int maxPosY)
    {
        Individual returnable;
        returnable = createNewIndividual(posVal,maxPosX, maxPosY);
        speed += 0.5;

        return returnable;
    }

    private Individual createNewIndividual(int posInt, int maxPosX, int maxPosY)
    {

        double newSpeed = speed /2;
        double newSize = size /2;
        int newSense = sense /2;

        double newEnergySupply =  ((newSpeed * newSpeed) * (newSize * newSize * newSize)) + newSense;

        int newIndividualPosX;
        int newIndividualPosY;

        if(posInt == 0)         //Right
        {
            newIndividualPosX = individualPosX + maxPosX;
            newIndividualPosY = individualPosY;

        }
        else if(posInt == 1)        //Left
        {
            newIndividualPosX =  individualPosY - maxPosX;
            newIndividualPosY = individualPosY;
        }
        else if(posInt == 2)            //Up
        {
            newIndividualPosX = individualPosX;
            newIndividualPosY = individualPosY - maxPosY;
        }
        else                            //Down
        {
            newIndividualPosX = individualPosX;
            newIndividualPosY = individualPosY + maxPosY;
        }

        replicateCount++;
        canReplicate = false;

        return new Individual( newSpeed, newSize, newSense, newIndividualPosX, newIndividualPosY);
    }

    public Color makeColor (){
        float r = (float) 0.0;
        float g = (float) 0.0;
        float b = (float) 0.0;

        if(speed < 1.0 && speed >= 0.5)         //Hellblau
        {
            b = (float) 1.0;
            g = (float) 1.0;
        }
        else if(speed < 0.5)                    //Blau
        {
            b = (float) 1.0;
        }
        else if(speed > 1.0 && speed <= 1.5)        //Gelb
        {
            g = (float) 1.0;
            r = (float) 1.0;
        }
        else if(speed == 1)                     //Grün
        {
            g = (float) 1.0;
        }
        else
        {
            r = (float) 1.0;                    //Rot
        }

        return new Color(r, g, b, 1.0);
    }
}
